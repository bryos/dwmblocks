#!/bin/sh
ICON=""
BAT_DIR="/sys/class/power_supply/BAT0"

# pretty print battery percentage with \x0c and \x0b. Insert raw bytes
# in emacs with 'M-: (insert-byte ?\x0c 1) RET'
pp_printf() {
    case $# in
        3) printf " B:%s%% (%s:%.2d) " "$1" "$2" "$3" ;;
        *) printf " %s:%s%% " "$1" "$2" ;;
    esac
}

read -r capacity < "$BAT_DIR/capacity"
read -r status < "$BAT_DIR/status"

if [ "$status" = "Full" ]; then
    pp_printf "AC" "$capacity"
    exit 0
elif [ "$status" = "Charging" ]; then
    [ "$capacity" -ge 85 ] && pp_printf "AC" "$capacity" && exit 0
    printf "AC:%s%%" "$capacity"
else
    [ "$capacity" -gt 15 ] && printf "B:%s%%" "$capacity" && exit 0
    read -r rate < "$BAT_DIR"/current_now
    read -r val < "$BAT_DIR"/charge_now
    hr="$(( val / rate ))"
    mn="$(( (val % rate * 60) / rate ))"
    pp_printf "$capacity" "$hr" "$mn" && exit 0
fi
